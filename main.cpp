#include "opencv2/opencv.hpp"
#include <thread>
#include <iostream>
#include <string>
#include "tinyxml2.h"
#include <opencv2/core.hpp>
#include <opencv2/highgui.hpp>
#include <opencv2/imgproc.hpp>

struct CameraInfo{
    cv::Size Resolution;
    cv::Mat Intrinsic;
    cv::Mat Distortion;
    cv::Mat Extrinsic;
};

float mStep = 0.4;

void StreamThread(cv::VideoCapture* stream,std::list< cv::Mat> *vFrames, bool *isRun)
{
    cv::Mat image;
    while (*isRun){
        (*stream) >> image;
        while((*vFrames).size() > 3){
            (*vFrames).pop_front();
        }
        (*vFrames).push_back(image.clone());
//        printf("%d mat stacked \n", (*vFrames).size());
    }
}

cv::VideoCapture createStream(std::string streamUri){
    cv::VideoCapture stream;
    stream.open(streamUri);
    //open check
    if (!stream.isOpened()){
        std::cerr << "Stream open failed : " << streamUri << std::endl;
        return EXIT_FAILURE;
    }
    return stream;
}


int main(int argc, char *argv[])
{
//    float tmpExtrinsic[4][4] = {{-6.96199059e-01, 5.79509318e-01, 2.75987685e-02, 1.19916208e-01}, \
//                                 {2.15769663e-01, 1.24255136e-01, -5.09205639e-01, 5.42357683e-01}, \
//                                 {-6.10200942e-01, -2.23487407e-01, -2.11545438e-01, 5.66596746e+00}, \
//                                 {0., 0., 0., 1.}};
//    float tmpIntrinsic[3][3] = {{9.87734558e+02, 0., 6.93573486e+02}, \
//                                 {0., 9.80822998e+02, 2.53205688e+02}, \
//                                 {0., 0., 1.}};
//    float tmpDistortion[1][5] = {{-3.66910696e-01, 3.93091798e-01, 1.56810209e-02, -8.53620004e-03, 0.}};

//    cv::Mat tExtrinsic(4,4,CV_32F);
//    cv::Mat tIntrinsic(3,3,CV_32F);
//    cv::Mat tDistortion(1,5,CV_32F);

//    for(int i = 0; i<4; i++){
//        for(int j=0; j<4; j++){
//            tExtrinsic.at<float>(i,j) = tmpExtrinsic[i][j];
//        }
//    }
//    for(int i = 0; i<3; i++){
//        for(int j=0; j<3; j++){
//            tIntrinsic.at<float>(i,j) = tmpIntrinsic[i][j];
//        }
//    }
//    for(int i = 0; i<1; i++){
//        for(int j=0; j<5; j++){
//            tDistortion.at<float>(i,j) = tmpDistortion[i][j];
//        }
//    }
    bool isRun = true;
    std::string streamUri = "rtsp://admin:a1234567@192.168.4.201:554/MPEG/ch1/main/av_stream";
    cv::VideoCapture stream = createStream(streamUri);
    std::list< cv::Mat> vFrames;
    std::thread(StreamThread, &stream, &vFrames, &isRun).detach();

//    CAM0
//    cv::Mat tExtrinsic = (cv::Mat_<float>(4, 4) << 0.018556349, 0.80738533, 0.034833878, -1.2901608,
//                                                    0.33384722, -0.058195498, -0.68921626, 0.59684229,
//                                                    -0.84784591, 0.25213557, -0.27062279, 5.464942,
//                                                    0, 0, 0, 1);
//    cv::Mat tIntrinsic = (cv::Mat_<float>(3, 3) << 2236.2878273059746, 0.000000, 798.237053768776,
//                                                  0.000000, 2235.8476618407176, 408.7268679699223,
//                                                  0.000000, 0.000000, 1.000000);
//    cv::Mat tDistortion = (cv::Mat_<float>(1, 5) << -0.26775702623985737, 0.09083133617006492, 0.005548189920131682,
//                                                    -0.0011332862975525098, 0.000000);

//    CAM1
//    cv::Mat tExtrinsic = (cv::Mat_<float>(4, 4) << -0.567424423625418, 0.7842897012399355, 0.03630299426002351, -0.04717853961560392,
//                                                    0.2986562848563279, 0.1793883494317939, -0.6597276318271064, 0.06980341358319891,
//                                                    -0.6476688493997806, -0.2674689091520794, -0.3360218608719372, 5.536928409128119,
//                                                    0, 0, 0, 1);
//    cv::Mat tIntrinsic = (cv::Mat_<float>(3, 3) << 1023.8942230374165, 0.000000, 650.539768256422,
//                          0.000000, 1022.4374177110845, 320.39779200148735,
//                          0.000000, 0.000000, 1.000000);
//    cv::Mat tDistortion = (cv::Mat_<float>(1, 5) << -0.31326111843288285, 0.09915391138552988, 0.005174073647506344,
//                                                      -0.0011900743709920612, 0.000000);


//    CAM2
//    cv::Mat tExtrinsic = (cv::Mat_<float>(4, 4) << -0.9013875664174821, 0.1349863272067231, -0.05821861178217691, 2.368199066286874,
//                                                    0.003979105602782224, 0.4108781426454952, -0.6391518355824428, 0.5113458005040388,
//                                                    0.1346313979732219, -0.7292379420390284, -0.3708971163085952, 3.741201971306995,
//                                                    0, 0, 0, 1);
//    cv::Mat tIntrinsic = (cv::Mat_<float>(3, 3) << 2607.6100019345818, 0.000000, 815.1224660399741,
//                          0.000000, 2604.270253481356, 453.406980767996,
//                          0.000000, 0.000000, 1.000000);
//    cv::Mat tDistortion = (cv::Mat_<float>(1, 5) << -0.2984231048495419, 0.27183767241850215, -0.0007651985076521456,
//                                                      -0.0010741560109565277, 0.000000);


//    CAM3
    cv::Mat tExtrinsic = (cv::Mat_<float>(4, 4) << -0.4783273245225012, -0.5650418822643261, 0.0429614605746651, 2.412734658194147,
                                                    -0.3303415651283717, 0.2180189000112943, -0.6804240044041225, 1.721434615842424,
                                                    0.7019390375647588, -0.5933174220187249, -0.2909409453965427, 2.284912127532839,
                                                    0, 0, 0, 1);
    cv::Mat tIntrinsic = (cv::Mat_<float>(3, 3) << 1130.8826950587395, 0.000000, 986.757138343483,
                          0.000000, 1133.0969129523194, 636.3782195104426,
                          0.000000, 0.000000, 1.000000);
    cv::Mat tDistortion = (cv::Mat_<float>(1, 5) << -0.30000845394094244, 0.08689646681641881, 0.0005040209364785389,
                                                      -0.0007970480367876766, 0.000000);





    std::cout<<"tExtrinsic"<<tExtrinsic<<std::endl;
    std::cout<<"tIntrinsic"<<tIntrinsic<<std::endl;
    std::cout<<"tDistortion"<<tDistortion<<std::endl;

    float RoomSize[2] = {4.0, 3.0}; //RoomSize(width, length) in meters
    float zPre = 1.6;

    std::vector<cv::Point2f> v2DPoses;
    std::vector<cv::Point3f> v3D;
    std::vector<cv::Point3f> v3DOrigin;
    v3DOrigin.push_back( cv::Point3f( 0.0, 0.0, 0.0 ) );
    v3DOrigin.push_back( cv::Point3f( 3.0, 0.0, 0.0 ) );
    v3DOrigin.push_back( cv::Point3f( 0.0, 3.0, 0.0 ) );
    v3DOrigin.push_back( cv::Point3f( 0.0, 0.0, 3.0 ) );


    for(int i=0; i*mStep<=RoomSize[0]; i++){
        for(int j=0; j*mStep<=RoomSize[1]; j++){
            cv::Point3f v3Dpos(i*mStep, j*mStep, zPre);
//            std::cout<<"v3Dpos"<<v3Dpos<<std::endl;
            v3D.push_back(v3Dpos);
        }
    }

    cv::Mat mRvec, mTvec;
    cv::Rodrigues(tExtrinsic(cv::Rect(0,0,3,3)),mRvec);
    mTvec = tExtrinsic(cv::Rect(3,0,1,3)).clone();

    std::vector<cv::Point2f> v2DOrigin;
    cv::projectPoints(v3DOrigin,mRvec,mTvec,tIntrinsic,tDistortion,v2DOrigin);
    cv::projectPoints(v3D,mRvec,mTvec,tIntrinsic,tDistortion,v2DPoses);
    //std::cout<<"v2Dpos"<<v2DPoses<<std::endl;


    cv::Mat mOriginalFrame, resizedFrame;
    while(1){
//        cv::Mat mExtrinsic=cv::Mat::zeros(4,4,CV_32FC1);

        if (vFrames.size()>1){
            mOriginalFrame = vFrames.front().clone();
            vFrames.pop_front();
        }
        if (!mOriginalFrame.data)
            continue;

        for(int i = 0; i < v2DPoses.size(); i++){
            cv::Point2f tmp = v2DPoses[i];
            if(tmp.x > 1920 || tmp.y > 1080) {continue;}
            cv::circle(mOriginalFrame,tmp,20,cv::Scalar(0,0,255),5);
        }

        cv::line(mOriginalFrame, v2DOrigin[0], v2DOrigin[1], CV_RGB(255,0,0), 2 );
        cv::line(mOriginalFrame, v2DOrigin[0], v2DOrigin[2], CV_RGB(0,255,0), 2 );
        cv::line(mOriginalFrame, v2DOrigin[0], v2DOrigin[3], CV_RGB(0,0,255), 2 );

        cv::resize(mOriginalFrame,resizedFrame,cv::Size(640,360));
        cv::imshow("Cam",resizedFrame);
        uchar k = cv::waitKey(30);

    }

    return 0;
}

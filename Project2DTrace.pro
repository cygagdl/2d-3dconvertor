#-------------------------------------------------
#
# Project created by QtCreator 2017-10-13T20:46:39
#
#-------------------------------------------------

QT       += core

QT       -= gui

TARGET = Project2DTrace
CONFIG   += console
CONFIG   += c++11
CONFIG   -= app_bundle

TEMPLATE = app


SOURCES += main.cpp \
    tinyxml2.cpp

HEADERS += \
    tinyxml2.h

LIBS += /usr/local/lib/*opencv*.so
